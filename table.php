<?php
function generate_table($header, $rows) {
  $hc = count($header);
  $rc = count($rows[0]);
  $rr = count($rows);
  if ($hc == 0 and $rc == 0) {
    return '';
  }
  if (($hc > 0 and $hc != $rc) and ($rc > 0 and $rc !=$hc) ) {
    return '';
  }
  $htm = '<!DOCTYPE html>
  <html>
    <head>
      <title>Your Table</title>
     </head>
     <body>
     <table border = "1" cellpadding = "5" cellspacing = "5"';
  if ($hc == 0 and $rc != 0) {
    $htm .= ' class="TFtable1">';
    for ($i = 0; $i < $rr; $i++) {
      $htm .= '<tr>';
      for ($j = 0; $j < $rc; $j++) {
        $htm .= '<td>' . $rows[$i][$j] . '</td>';
      }
      $htm .= '</tr>';
    }
    $htm .= '</table>
  </body>
</html>';
  return $htm;
  }
  elseif ($hc != 0 and $rc == 0) {
    $htm .= 'class="TFtable2">
    <tr>';
    foreach ($header as $h) {
      $htm .= '<th>' . $h . '</th>';
    }
    $htm .= '</tr>';
    $htm .= '</table>
  </body>
</html>';
  return $htm;
  }
  else {
    if ($hc != 0) {
      $htm .= ' class="TFtable3">
      <tr>';
      foreach ($header as $h) {
        $htm .= '<th>' . $h . '</th>';
      }
      $htm .= '</tr>';
    }
    if ($rc != 0) {
      for ($i = 0; $i < $rr; $i++) {
        $htm .= '<tr>';
        for ($j = 0; $j < $rc; $j++) {
          $htm .= '<td>' . $rows[$i][$j] . '</td>';
        }
        $htm .= '</tr>';
      }
    }
    $htm .= '</table>
        </body>
        </html>';
    return $htm;
  }
}

function generate_data($a, $b) {
  for ($i = 0; $i < $a; $i++) {
    for ($j = 0; $j < $b; $j++) {
      $rows[$i][$j] = rand(1,100); 
    }
   }
  return $rows;
}

$rows = generate_data(4,3);
$header = array("column 1", "column 2", "column 3");
echo generate_table($header, $rows);
?> 

<style type="text/css">

  .TFtable1{
		width:100%; 
		border-collapse:collapse; 
	}

  .TFtable2{
		width:100%; 
		border-collapse:collapse; 
	}

  .TFtable3{
		width:100%; 
		border-collapse:collapse; 
	}

  .TFtable1 th{
		background: #1a94c7;
	}

  .TFtable2 th{
		background: #1a94c7;
	}
  
  .TFtable3 th{
		background: #1a94c7;
	}

	.TFtable1 tr:nth-child(even) { 
		background:  #a7c1c4;
	}

  .TFtable3 tr:nth-child(odd) { 
		background: #a7c1c4;
	}
  
</style>


